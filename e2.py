'''EJERCICIOS CAPITULO 5'''
#__author__ = "Juan     Alvarez"
#__email_ = "juan.v.alvarez@unl.edu.ec"
'''Ejercicio 2: Escribe otro programa que pida una lista de números como
la anterior y al final muestre por pantalla el máximo y mínimo de los
números, en vez de la media.'''
contador = 0
lista = []
total = 0


while True:

    valor = input("Introduce un número entero (o 'fin' para terminar): ")
    lista.append(valor)
    if valor.lower() in "fin":
        break
    try:
        total += float (valor)
        contador  += 1
        max1 = max (lista)
        min1 = min (lista)
    except:
        print("Valor introducido incorrecto. Ingrese el valor nuevamente..")

print("El total es->: ", total)
print("Haz introducido->: ", contador, " numeros")
print("Lista de numeros->:", lista)
print("El numero mayor ingresado es->: ", float(max1))
print("El numero menor ingresado es->: ", float(min1))
